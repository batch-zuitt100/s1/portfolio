# Web Developer Portfolio - Code Review Session

- Clone this project to your **firstname-lastname** folder.
- Refer to the [Deployment Steps](https://gitlab.com/help/user/project/pages/getting_started/pages_new_project_template.md) indicated here.

## Folder Structure

- Put all your resources (HTML and CSS files) inside the `public` folder.
- Inside the `public` folder, create an `assets` folder and add the `css` and `images` folder inside it. 
- Inside the `css` folder, add an `style.css` file.
- Add all images inside the `images` folder.

## Installation Checks

Execute each line to check for the installation status of each command-line tool.

```
git --version
node -v (for Windows)
nodejs -v (for Linux)
npm -v
heroku -v
```

## Basic CLI Commands

<table>
    <tr>
        <th>Command</th>
        <th>Description</th>
    </tr>
    <tr>
        <td><code>ls</code></td>
        <td>checks the list of items inside the directory</td>
    </tr>
    <tr>
        <td><code>cd</code></td>
        <td>change directory</td>
    </tr>
    <tr>
        <td><code>cd ..</code></td>
        <td>one folder up; go back to the parent folder</td>
    </tr>
    <tr>
        <td><code>pwd</code></td>
        <td>check the current location in the system</td>
    </tr>
    <tr>
        <td><code>mkdir &lt;folder_name&gt;</code></td>
        <td>create a directory (folder)</td>
    </tr>
    <tr>
        <td><code>touch &lt;file_name&gt;</code></td>
        <td>create a file</td>
    </tr>
</table>